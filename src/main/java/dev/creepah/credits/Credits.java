package dev.creepah.credits;

import dev.creepah.credits.cmd.*;
import dev.creepah.credits.misc.CreditsManager;
import lombok.Getter;
import me.twister915.corelite.plugin.CLPlugin;
import me.twister915.corelite.plugin.UsesFormats;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@UsesFormats
public final class Credits extends CLPlugin {

    @Getter
    private static Credits instance;

    @Getter
    private FileConfiguration storageYML;

    public Map<UUID, Integer> credits;

    @Override
    protected void onModuleEnable() throws Exception {
        instance = this;
        credits = new HashMap<>();

        storageYML = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "storage.yml"));
        CreditsManager.getInstance().load();

        registerCommand(new CreditsCommand());
        registerCommand(new RedeemCommand());
        registerCommand(new GiveCreditsCommand());
        registerCommand(new TakeCreditsCommand());
        registerCommand(new UseCommand());
        registerListener(new CreditsManager());
    }

    @Override
    protected void onModuleDisable() throws Exception {
        saveStorageYML();
    }

    public void saveStorageYML() throws IOException {
        CreditsManager.getInstance().save();
        storageYML.save(new File(getDataFolder(), "storage.yml"));
    }
}
