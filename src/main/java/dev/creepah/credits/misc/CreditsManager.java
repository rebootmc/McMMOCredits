package dev.creepah.credits.misc;

import com.gmail.nossr50.api.ExperienceAPI;
import com.gmail.nossr50.datatypes.player.McMMOPlayer;
import com.gmail.nossr50.datatypes.skills.SkillType;
import com.gmail.nossr50.util.player.UserManager;
import dev.creepah.credits.Credits;
import lombok.Getter;
import me.twister915.corelite.CLPlayerKnife;
import me.twister915.corelite.command.ArgumentRequirementException;
import me.twister915.corelite.command.CommandException;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CreditsManager implements Listener {

    @Getter
    private static CreditsManager instance = new CreditsManager();

    public Map<UUID, Integer> credits = Credits.getInstance().credits;

    public void load() {
        for (String str : Credits.getInstance().getStorageYML().getStringList("credits")) {
            String[] s = str.split(":");
            credits.put(UUID.fromString(s[0]), Integer.parseInt(s[1]));
        }
    }

    public void save() {
        List<String> saveList = new ArrayList<>();
        for (UUID uuid : credits.keySet()) saveList.add(uuid + ":" + credits.get(uuid));
        Credits.getInstance().getStorageYML().set("credits", saveList);
        credits.clear();
    }

    public void redeem(Player player, String args[]) throws CommandException {
        if (args.length < 2) throw new ArgumentRequirementException("Invalid arguments! Usage: /credits redeem <skill> <amount>");
        SkillType type;
        try {
            type = SkillType.valueOf(args[0].toUpperCase());
        } catch (Exception e) { type = null; }

        Integer amount = parse(args[1]);
        
        if (type == null || amount == -1)
            throw new CommandException("Invalid skill type or amount specified!");

        McMMOPlayer p = UserManager.getPlayer(player);
        if (amount > p.getSkillLevel(type)) {
            player.sendMessage(Credits.getInstance().formatAt("too-high").with("level", p.getSkillLevel(type)).get());
            return;
        }

        ExperienceAPI.addLevel(player, type.getName(), -amount);
        if (credits.containsKey(player.getUniqueId())) credits.replace(player.getUniqueId(), credits.get(player.getUniqueId()) + amount);
        else credits.put(player.getUniqueId(), amount);
        player.sendMessage(Credits.getInstance().formatAt("credits-added").with("amount", amount).with("type", type.getName()).get());
    }
    
    public void use(Player player, String args[]) throws CommandException {
        if (args.length < 2) throw new ArgumentRequirementException("Invalid arguments! Usage: /credits use <skill> <amount>");
        SkillType type;
        try {
            type = SkillType.valueOf(args[0].toUpperCase());
        } catch (Exception e) { type = null; }

        Integer amount = parse(args[1]);

        if (type == null || amount == -1)
            throw new CommandException("Invalid skill type or amount specified!");

        if (amount > credits.get(player.getUniqueId())) {
            player.sendMessage(Credits.getInstance().formatAt("not-enough-credits").get());
            return;
        }

        if ((ExperienceAPI.getLevel(player, type.getName()) + amount) > ExperienceAPI.getLevelCap(type.getName())) {
            player.sendMessage(Credits.getInstance().formatAt("beyond-level-cap").get());
            return;
        }

        ExperienceAPI.addLevel(player, type.getName(), amount);
        credits.replace(player.getUniqueId(), credits.get(player.getUniqueId()) - amount);
        player.sendMessage(Credits.getInstance().formatAt("skill-added")
                .with("amount", amount)
                .with("skill", type.getName())
                .get());
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getView().getType() == InventoryType.ANVIL) {
            ItemStack item = event.getView().getItem(0);
            if (item != null && item.getType() == Material.PAPER) {
                if (item.hasItemMeta()) {
                    event.setCancelled(true);
                    CLPlayerKnife.$((Player) event.getWhoClicked()).playSoundForPlayer(Sound.NOTE_BASS);
                }
            }
        }
    }

    public int parse(String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException ignored) {}
        return -1;
    }
}
