package dev.creepah.credits.cmd;

import dev.creepah.credits.misc.CreditsManager;
import me.twister915.corelite.command.CommandException;
import me.twister915.corelite.command.CommandPermission;
import org.bukkit.entity.Player;

@CommandPermission("credits.use")
public class UseSubCmd extends CCommand {

    public UseSubCmd() {
        super("use");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        CreditsManager.getInstance().use(player, args);
    }
}
