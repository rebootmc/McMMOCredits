package dev.creepah.credits.cmd;

import dev.creepah.credits.Credits;
import me.twister915.corelite.command.ArgumentRequirementException;
import me.twister915.corelite.command.CommandException;
import me.twister915.corelite.command.CommandMeta;
import me.twister915.corelite.command.CommandPermission;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import java.util.Map;
import java.util.UUID;

@CommandPermission("credits.take")
@CommandMeta(description = "Alt. of /credits take")
public class TakeCreditsCommand extends CCommand {

    public TakeCreditsCommand() {
        super("takecredits");
    }

    @Override
    protected void handleCommandUnspecific(CommandSender sender, String[] args) throws CommandException {
        if (args.length < 2) throw new ArgumentRequirementException("Invalid arguments! Usage: /credits take <player> <amount>");

        Map<UUID, Integer> credits = Credits.getInstance().credits;
        OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
        int amount = parse(args[1]);

        if (amount == -1) throw new CommandException("Invalid amount! Please enter a number!");

        if (credits.get(target.getUniqueId()) == null || credits.get(target.getUniqueId()) == 0)
            throw new CommandException(target.getName() + " has no credits to take!");
        else {
            if (credits.get(target.getUniqueId()) < amount) amount = credits.get(target.getUniqueId());
            credits.replace(target.getUniqueId(), credits.get(target.getUniqueId()) - amount);
        }

        sender.sendMessage(Credits.getInstance().formatAt("credits-taken")
                .with("target", target.getName())
                .with("amount", amount)
                .with("new", credits.get(target.getUniqueId()))
                .get());
    }

    public int parse(String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException ignored) {}
        return -1;
    }
}
