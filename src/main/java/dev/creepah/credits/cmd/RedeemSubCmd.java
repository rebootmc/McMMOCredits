package dev.creepah.credits.cmd;

import dev.creepah.credits.misc.CreditsManager;
import me.twister915.corelite.command.CommandException;
import me.twister915.corelite.command.CommandPermission;
import org.bukkit.entity.Player;

@CommandPermission("credits.redeem")
public class RedeemSubCmd extends CCommand {

    public RedeemSubCmd() {
        super("redeem");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        CreditsManager.getInstance().redeem(player, args);
    }
}
