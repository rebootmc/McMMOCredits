package dev.creepah.credits.cmd;

import dev.creepah.credits.Credits;
import me.twister915.corelite.CLPlayerKnife;
import me.twister915.corelite.command.CLCommand;
import me.twister915.corelite.command.CommandException;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class CCommand extends CLCommand {

    protected CCommand(String name) {
        super(name);
    }

    protected CCommand(String name, CCommand... subCommands) {
        super(name, subCommands);
    }

    @Override
    protected void handleCommandException(CommandException ex, String[] args, CommandSender sender) {
        if (sender instanceof Player) CLPlayerKnife.$((Player) sender).playSoundForPlayer(Sound.NOTE_BASS);
        sender.sendMessage(Credits.getInstance().formatAt("error-message").with("message", ex.getMessage()).get());
    }
}
