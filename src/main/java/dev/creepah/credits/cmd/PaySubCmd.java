package dev.creepah.credits.cmd;

import dev.creepah.credits.Credits;
import me.twister915.corelite.command.ArgumentRequirementException;
import me.twister915.corelite.command.CommandException;
import me.twister915.corelite.command.CommandPermission;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.UUID;

@CommandPermission("credits.pay")
public class PaySubCmd extends CCommand {

    public PaySubCmd() {
        super("pay");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        if (args.length < 2) throw new ArgumentRequirementException("Invalid arguments! Usage: /credits pay <player> <amount>");

        Map<UUID, Integer> credits = Credits.getInstance().credits;
        Player target = Bukkit.getPlayer(args[0]);

        if (target == null)
            throw new CommandException("The player " + args[0] + " could not be found!");

        Integer amount;
        try {
            amount = Integer.parseInt(args[1]);
        } catch (NumberFormatException ex) {
            throw new CommandException("That is not a number!");
        }

        if (credits.get(player.getUniqueId()) == null || amount > credits.get(player.getUniqueId()))
            throw new CommandException("You don't have enough credits to do this!");

        if (credits.get(target.getUniqueId()) == null) credits.put(target.getUniqueId(), 0);

        credits.replace(player.getUniqueId(), credits.get(player.getUniqueId()) - amount);
        credits.replace(target.getUniqueId(), credits.get(target.getUniqueId()) + amount);

        player.sendMessage(Credits.getInstance().formatAt("payment-sent")
                .withModifier("target", target.getName())
                .withModifier("amount", amount).get());

        target.sendMessage(Credits.getInstance().formatAt("payment-received")
                .withModifier("player", player.getName())
                .withModifier("amount", amount).get());
    }
}
