package dev.creepah.credits.cmd;

import dev.creepah.credits.Credits;
import dev.creepah.credits.misc.CreditsManager;
import me.twister915.corelite.command.ArgumentRequirementException;
import me.twister915.corelite.command.CommandException;
import me.twister915.corelite.command.CommandPermission;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.Map;
import java.util.UUID;

@CommandPermission("credits.convert")
public class ConvertSubCmd extends CCommand {

    public ConvertSubCmd() {
        super("convert");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        if (args.length == 0) throw new ArgumentRequirementException("Invalid arguments! Usage: /credits convert <file name>");

        String fileName = args[0].toLowerCase();

        FileConfiguration config = YamlConfiguration.loadConfiguration(new File(Credits.getInstance().getDataFolder(), fileName));
        Map<UUID, Integer> credits = CreditsManager.getInstance().credits;

        int count = 0;
        for (String key : config.getKeys(false)) {
            credits.put(UUID.fromString(key), config.getInt(key + ".credits"));
            count++;
        }
        player.sendMessage(Credits.getInstance().formatAt("convert").with("file", fileName).with("count", count).get());
    }
}
