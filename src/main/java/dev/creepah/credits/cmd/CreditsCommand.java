package dev.creepah.credits.cmd;

import dev.creepah.credits.Credits;
import me.twister915.corelite.command.CommandException;
import me.twister915.corelite.command.CommandMeta;
import me.twister915.corelite.command.CommandPermission;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

@CommandPermission("credits.view")
@CommandMeta(description = "Base command for mcMMOCredits")
public class CreditsCommand extends CCommand {

    public CreditsCommand() {
        super("credits",
                new RedeemSubCmd(),
                new GiveSubCmd(),
                new TakeSubCmd(),
                new UseSubCmd(),
                new ConvertSubCmd(),
                new PaySubCmd());
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        OfflinePlayer target;
        if (args.length == 0) target = player;
        else target = Bukkit.getOfflinePlayer(args[0]);

        int credits;
        Integer c = Credits.getInstance().credits.get(target.getUniqueId());
        if (c != null) {
            credits = c;
        } else {
            credits = 0;
        }

        player.sendMessage(Credits.getInstance().formatAt("credits").with("credits", credits).with("target", target.getName()).get());
    }

}
