package dev.creepah.credits.cmd;

import dev.creepah.credits.misc.CreditsManager;
import me.twister915.corelite.command.CommandException;
import me.twister915.corelite.command.CommandMeta;
import me.twister915.corelite.command.CommandPermission;
import org.bukkit.entity.Player;

@CommandPermission("credits.redeem")
@CommandMeta(description = "Alt. to /credits redeem")
public class RedeemCommand extends CCommand {

    public RedeemCommand() {
        super("redeem");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        CreditsManager.getInstance().redeem(player, args);
    }
}
