package dev.creepah.credits.cmd;

import dev.creepah.credits.Credits;
import me.twister915.corelite.command.ArgumentRequirementException;
import me.twister915.corelite.command.CommandException;
import me.twister915.corelite.command.CommandPermission;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import java.util.Map;
import java.util.UUID;

@CommandPermission("credits.give")
public class GiveSubCmd extends CCommand {

    public GiveSubCmd() {
        super("give");
    }

    @Override
    protected void handleCommandUnspecific(CommandSender sender, String[] args) throws CommandException {
        if (args.length < 2) throw new ArgumentRequirementException("Invalid arguments! Usage: /credits give <player> <amount>");

        Map<UUID, Integer> credits = Credits.getInstance().credits;
        OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
        int amount = parse(args[1]);

        if (amount == -1) throw new CommandException("Invalid amount! Please enter a number!");

        if (credits.get(target.getUniqueId()) == null) credits.put(target.getUniqueId(), amount);
        else credits.replace(target.getUniqueId(), credits.get(target.getUniqueId()) + amount);

        sender.sendMessage(Credits.getInstance().formatAt("credits-given")
                .with("target", target.getName())
                .with("amount", amount)
                .with("new", credits.get(target.getUniqueId()))
                .get());
    }

    public int parse(String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException ignored) {}
        return -1;
    }
}
